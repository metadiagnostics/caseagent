package io.saperi.virtualhealth.caseagent.handlers;

import io.saperi.common.communications.interfaces.IPubSubEventCallback;
import io.saperi.common.communications.interfaces.IPubSubProvider;
import io.saperi.common.communications.listeners.TypeMappedListener;
import io.saperi.virtualhealth.caseagent.components.EventFactory;
import io.saperi.virtualhealth.caseagent.components.SharedContext;
import io.saperi.virtualhealth.caseagent.shared.event.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * This class handles all events on the Private Input Channel
 */
@Slf4j
@Component
public class PrivateInputHandler extends TypeMappedListener
{
    private SharedContext ctx;
    private EventFactory eventFactory;
    private IPubSubProvider pubSubSrv;

    public PrivateInputHandler(SharedContext ctx, EventFactory eventFactory, IPubSubProvider pubSubSrv) {
        this.ctx = ctx;
        this.eventFactory = eventFactory;
        this.pubSubSrv = pubSubSrv;
    }

    @PostConstruct
    public void setupHandlers()
    {

        this.addMapping(CaseInfoRequest.class, new handleCaseInfoRequest());
        this.addMapping(CaseAgentConfirmAliveRequest.class, new handleCaseAgentConfirmAliveRequest());
        this.addMapping(DeviceStatusRequest.class,new TypeMappedListener.genericUnimplementedHandler());
        this.addMapping(DeviceDatafeedRequest.class,new TypeMappedListener.genericUnimplementedHandler());
        this.addMapping(DeviceChangeRequest.class,new TypeMappedListener.genericUnimplementedHandler());
    }

    class handleCaseInfoRequest implements IPubSubEventCallback {

        @Override
        public void handleEvent(String topic, String type, Object obj) {
            CaseInfoRequest evt = (CaseInfoRequest) obj;
            log.info("Got a Request for Case Info Event");
            CaseInfo info = eventFactory.createCaseInfoEvent(ctx);
            pubSubSrv.publishToTopic(ctx.getRootCATopicOutput(),info);
            log.info("Responded to a Request for Case Info Event");
        }
    }

    class handleCaseAgentConfirmAliveRequest implements IPubSubEventCallback {

        @Override
        public void handleEvent(String topic, String type, Object obj) {
            CaseAgentConfirmAliveRequest evt = (CaseAgentConfirmAliveRequest) obj;
            if (evt.getAgentId() != null && ctx.getCaseAgentId().compareTo(evt.getAgentId())!=0 )
            {
                log.warn("Got an invalid request to confirm we are alive. Our Id = {}, Check Id = {}, Caller = {} ",evt.getAgentId(),ctx.getCaseAgentId(),evt.getRequesterAgentId());
                return;
            }

            CaseAgentAlive info = eventFactory.createCaseAgentAliveEvent(ctx);
            pubSubSrv.publishToTopic(ctx.getKeepAliveTopic(),info);
            log.info("Responded to a Request for Case Agent Alive Event");
        }
    }




}
