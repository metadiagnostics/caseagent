package io.saperi.virtualhealth.caseagent.components;

import io.saperi.virtualhealth.caseagent.shared.event.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class EventFactory {

    public CaseAgentStarted createCaseAgentStartedEvent(SharedContext context)
    {
        CaseAgentStarted evt = new CaseAgentStarted();
        evt.setCaseAgentId(context.getCaseAgentId());
        evt.setInputTopic(context.getRootCATopicInputTopic());
        evt.setOutputTopic(context.getRootCATopicOutput());
        evt.setStarted(context.getStarted());
        return evt;
    }

    public CaseAgentStopped createCaseAgentStoppedEvent(SharedContext context)
    {
        CaseAgentStopped evt = new CaseAgentStopped();
        evt.setCaseAgentId(context.getCaseAgentId());
        evt.setStopped(context.getStarted());
        return evt;
    }

    public CaseInfo createCaseInfoEvent(SharedContext context)
    {
        CaseInfo evt = new CaseInfo();
        evt.setStarted(context.getStarted());
        evt.setCaseAgentId(context.getCaseAgentId());
        evt.setDemographics(context.getCaseInfo().getPatientInfo());
        evt.setOutputTopic(context.getRootCATopicOutput());
        evt.setInputTopic(context.getRootCATopicInputTopic());
        return evt;
    }
    public CensusResponse createCensusResponseEvent(SharedContext context)
    {
        CensusResponse evt = new CensusResponse();
        evt.setStarted(context.getStarted());
        evt.setCaseAgentId(context.getCaseAgentId());
        evt.setDemographics(context.getCaseInfo().getPatientInfo());
        evt.setOutputTopic(context.getRootCATopicOutput());
        evt.setInputTopic(context.getRootCATopicInputTopic());
        evt.setEndpoint(context.getRootEndpoint());
        return evt;
    }

    public CaseAgentAlive createCaseAgentAliveEvent(SharedContext context)
    {
        CaseAgentAlive evt = new CaseAgentAlive();
        evt.setCaseAgentId(context.getCaseAgentId());
        return evt;
    }
}
