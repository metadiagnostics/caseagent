package io.saperi.virtualhealth.caseagent.handlers;

import io.saperi.common.communications.interfaces.IPubSubEventCallback;
import io.saperi.common.communications.interfaces.IPubSubProvider;
import io.saperi.common.communications.listeners.TypeMappedListener;
import io.saperi.virtualhealth.caseagent.components.EventFactory;
import io.saperi.virtualhealth.caseagent.components.SharedContext;
import io.saperi.virtualhealth.caseagent.shared.event.CaseAgentStarted;
import io.saperi.virtualhealth.caseagent.shared.event.CaseInfo;
import io.saperi.virtualhealth.caseagent.shared.event.CensusRequest;
import io.saperi.virtualhealth.caseagent.shared.event.CensusResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Slf4j
@Component
public class ManagementChannelHandler extends TypeMappedListener {
    private SharedContext ctx;
    private EventFactory eventFactory;
    private IPubSubProvider pubSubSrv;

    @PostConstruct
    public void setupHandlers()
    {
        this.addMapping(CaseAgentStarted.class, new handleCaseAgentStarted());
    }

    class handleCaseAgentStarted implements IPubSubEventCallback {

        @Override
        public void handleEvent(String topic, String type, Object obj) {
            CaseAgentStarted evt = (CaseAgentStarted) obj;
            log.info("Got a CaseAgent Started Event");
        }
    }

    class handleCensusRequest implements IPubSubEventCallback {

        @Override
        public void handleEvent(String topic, String type, Object obj) {
            CensusRequest evt = (CensusRequest) obj;
            log.info("Got a Census Request Event");
            CensusResponse info = eventFactory.createCensusResponseEvent(ctx);
            pubSubSrv.publishToTopic(ctx.getRootCATopicOutput(),info);
            log.info("Responded to a Census Request Event");
        }
    }


}
