package io.saperi.virtualhealth.caseagent.controller;


import io.saperi.common.communications.interfaces.IPubSubProvider;
import io.saperi.virtualhealth.caseagent.components.SharedContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@CrossOrigin(origins = "*")
public class TestController {

    private SharedContext context;
    //Found via the beans.xml and properties file
    private IPubSubProvider pubSubService;

    public TestController(SharedContext context, IPubSubProvider pubSubService) {
        this.context = context;
        this.pubSubService = pubSubService;
    }

    @GetMapping("/test/publish")
    public void testBoot(String topic, String msgText)
    {
        pubSubService.publishToTopic(topic,msgText);
    }


}
