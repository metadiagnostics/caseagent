package io.saperi.virtualhealth.caseagent.jobs;

import io.saperi.common.communications.interfaces.IPubSubProvider;
import io.saperi.virtualhealth.caseagent.components.EventFactory;
import io.saperi.virtualhealth.caseagent.components.SharedContext;
import lombok.extern.slf4j.Slf4j;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class StillAlive implements Job {

    private final IPubSubProvider pubSubSrv;
    private final SharedContext context;
    private final EventFactory eventFactory;



    public StillAlive(IPubSubProvider pubSubSrv, SharedContext context, EventFactory eventFactory){
        this.pubSubSrv = pubSubSrv;
        this.context = context;
        this.eventFactory = eventFactory;
    }

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        //TODO: Post an alive meesage to the check channel
    }

}
