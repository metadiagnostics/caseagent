package io.saperi.virtualhealth.caseagent.service;


import io.saperi.common.communications.interfaces.IPubSubProvider;
import io.saperi.virtualhealth.caseagent.components.SharedContext;
import io.saperi.virtualhealth.caseagent.handlers.ManagementChannelHandler;
import io.saperi.virtualhealth.caseagent.handlers.PrivateInputHandler;
import io.saperi.virtualhealth.caseagent.shared.event.CaseAgentStarted;
import io.saperi.virtualhealth.caseagent.shared.event.CaseAgentStopped;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.ContextStoppedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import javax.annotation.PreDestroy;
import java.io.StringWriter;
import java.net.InetAddress;
import java.util.Map;

@Slf4j
@EnableWebMvc
@Configuration
@ImportResource("classpath:beans.xml")
@SpringBootApplication
@ComponentScan("io.saperi.virtualhealth.caseagent")
//We scan these components mainly to make sure there is at least one pubsub implementation available
@ComponentScan("io.saperi.common.communications")
public class ServiceApplication {

    //Found via the beans.xml and properties file
    private IPubSubProvider pubSubSrv;

    private SharedContext context;

    private ManagementChannelHandler mgmtHandler;
    private PrivateInputHandler inputHandler;



    public ServiceApplication(IPubSubProvider pubSubSrv, SharedContext context, ManagementChannelHandler mgmtHandler, PrivateInputHandler inputHandler) {
        this.pubSubSrv = pubSubSrv;
        this.context = context;
        this.mgmtHandler = mgmtHandler;
        this.inputHandler = inputHandler;
    }


    public static void main(String[] args) {
        SpringApplication.run(ServiceApplication.class, args);
    }

    @EventListener
    public void handleContextRefreshEvent(ContextRefreshedEvent evt)
    {
        log.info("Context Refreshed, "+evt.getApplicationContext().getApplicationName());
        initialize();
        log.info("Initialization  complete");

    }

    @EventListener
    public void handleContextStopped(ContextStoppedEvent evt)
    {
        log.info("Context Stopped, "+evt.getApplicationContext().getApplicationName());


    }

    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**");
            }
        };
    }

    private void initialize()
    {
        pubSubSrv.initialize();

        // Create Topics for Input/Output/Alive
        pubSubSrv.createTopic(context.getManagementTopic());
        //Temp Subscriptions
        pubSubSrv.subscribeToTopic(context.getManagementTopic(),mgmtHandler);
        //
        log.info("Creating input topic {}",context.getRootCATopicInputTopic());
        pubSubSrv.createTopic(context.getRootCATopicInputTopic());
        log.info("Creating root output topic {}",context.getRootCATopicOutput());
        pubSubSrv.createTopic(context.getRootCATopicOutput());
        log.info("Creating keep alive topic {}",context.getKeepAliveTopic());
        pubSubSrv.createTopic(context.getKeepAliveTopic());

        pubSubSrv.subscribeToTopic(context.getRootCATopicInputTopic(),inputHandler);


        //Publish Existence
        pubSubSrv.publishToTopic(context.getManagementTopic(),createCaseAgentStartedEvent());


        //TODO:   Start KeepAlive thread.

    }

    protected CaseAgentStarted createCaseAgentStartedEvent()
    {
        CaseAgentStarted evt = new CaseAgentStarted();
        evt.setCaseAgentId(context.getCaseAgentId());
        evt.setInputTopic(context.getRootCATopicInputTopic());
        evt.setOutputTopic(context.getRootCATopicOutput());
        evt.setStarted(context.getStarted());
        return evt;
    }
    protected CaseAgentStopped createCaseAgentStoppedEvent()
    {
        CaseAgentStopped evt = new CaseAgentStopped();
        evt.setCaseAgentId(context.getCaseAgentId());
        evt.setStopped(context.getStarted());
        return evt;
    }
    @PreDestroy
    public void tearDown()
    {
        pubSubSrv.publishToTopic(context.getManagementTopic(),createCaseAgentStoppedEvent());
        log.info("Shutting down....");
        pubSubSrv.cleanup();
    }


}
