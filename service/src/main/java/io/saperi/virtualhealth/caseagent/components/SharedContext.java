package io.saperi.virtualhealth.caseagent.components;

import com.fasterxml.uuid.Generators;
import io.saperi.common.communications.spring.tools.ServerDataService;
import io.saperi.virtualhealth.caseagent.data.CaseInfo;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.UUID;

@Component
public class SharedContext {

    private ServerProperties serverProperties;
    private ServerDataService serverDataService;

    @Value("${saperi.virtualhealth.domain}")
    private String domain;

    private Date started;
    private String caseAgentId;
    private String inputId;
    private String outputId;
    private CaseInfo caseInfo;
    private String rootCATopicInput;
    private String rootCATopicOutput;
    private String managementTopic;
    private String keepAliveTopic;
    private String rootEndpoint;

    SharedContext(ServerProperties serverProperties)
    {
        this.serverProperties = serverProperties;
        started = new Date();
        caseInfo = new CaseInfo();
    }

    @PostConstruct
    public void configure()
    {
        UUID uuid = Generators.timeBasedGenerator().generate();
        caseAgentId = uuid.toString();
        inputId = Generators.timeBasedGenerator().generate().toString();
        outputId = Generators.timeBasedGenerator().generate().toString();
        caseInfo.setCaseId(caseAgentId);
        rootCATopicInput = domain+"/"+inputId;
        rootCATopicOutput = domain+"/"+outputId;
        managementTopic = domain + "/CCMgmt";
        keepAliveTopic = domain +"/CCAlive";
    }

    public String getKeepAliveTopic() {
        return keepAliveTopic;
    }

    public String getManagementTopic() {
        return managementTopic;
    }

    public String getRootCATopicInputTopic()
    {
        return rootCATopicInput;
    }

    public String getCaseAgentId() {
        return caseAgentId;
    }

    public CaseInfo getCaseInfo() {
        return caseInfo;
    }

    public String getDomain() {
        return domain;
    }

    public String getRootCATopicOutput() {
        return rootCATopicOutput;
    }

    public String getRootEndpoint() {
        return serverDataService.getServerRoot();
    }

    public ServerProperties getServerProperties() {
        return serverProperties;
    }

    public Date getStarted() {
        return started;
    }

    public void setCaseAgentId(String caseAgentId) {
        this.caseAgentId = caseAgentId;
    }

    public void setCaseInfo(CaseInfo caseInfo) {
        this.caseInfo = caseInfo;
    }

}
