package io.saperi.virtualhealth.caseagent.controller;

import io.saperi.virtualhealth.caseagent.components.SharedContext;
import io.saperi.virtualhealth.caseagent.shared.event.Status;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@CrossOrigin(origins = "*")
public class ManagementController {

   private SharedContext context;

    public ManagementController(SharedContext context) {
        this.context = context;
        log.info("Started");
    }

    @GetMapping("/alive")
    public Status alive()
    {
        Status out = new Status();
        out.setTimeStarted(context.getStarted());
        return out;
    }
}
