package io.saperi.virtualhealth.caseagent.data;

import io.saperi.virtualhealth.caseagent.shared.data.PatientDemographics;
import lombok.Data;

@Data
public class CaseInfo {
    private String caseId;
    private PatientDemographics patientInfo;
}
