package io.saperi.virtualhealth.caseagent.shared.event;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.saperi.virtualhealth.caseagent.shared.data.PatientDemographics;
import io.saperi.virtualhealth.caseagent.shared.event.base.CaseCommon;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include. NON_NULL)
public class CaseOpened extends CaseCommon {
    private String caseId;
    private PatientDemographics demographics;
}
