package io.saperi.virtualhealth.caseagent.shared.event;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.saperi.virtualhealth.caseagent.shared.event.base.CaseCommonRequest;
import lombok.Data;

/**
 * Event that asks the case agent to generate a confirmation of aliveness
 * This event should be broadcast on the Private Input Channel for the Agent
 * The provision of an agentId is a as both a crosscheck and for possible use on the management channel
 */

@Data
@JsonInclude(JsonInclude.Include. NON_NULL)
public class CaseAgentConfirmAliveRequest extends CaseCommonRequest {
    String agentId;
}
