package io.saperi.virtualhealth.caseagent.shared.event;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.saperi.virtualhealth.caseagent.shared.data.PatientDemographics;
import io.saperi.virtualhealth.caseagent.shared.event.base.CaseCommon;
import lombok.Data;

import java.util.Date;

@Data
@JsonInclude(JsonInclude.Include. NON_NULL)
public class CaseInfo extends CaseCommon {
    private Date started;
    private String inputTopic;
    private String outputTopic;
    private String endpoint;
    private String caseId;
    private PatientDemographics demographics;
    //More to follow as needed
}
