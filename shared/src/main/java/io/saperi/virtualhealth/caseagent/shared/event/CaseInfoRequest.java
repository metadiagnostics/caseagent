package io.saperi.virtualhealth.caseagent.shared.event;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.saperi.virtualhealth.caseagent.shared.event.base.CaseCommonRequest;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include. NON_NULL)
public class CaseInfoRequest extends CaseCommonRequest {
}
