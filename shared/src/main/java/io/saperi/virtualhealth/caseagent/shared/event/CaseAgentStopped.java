package io.saperi.virtualhealth.caseagent.shared.event;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.saperi.virtualhealth.caseagent.shared.event.base.CaseCommon;
import lombok.Data;

import java.util.Date;

@Data
@JsonInclude(JsonInclude.Include. NON_NULL)
public class CaseAgentStopped extends CaseCommon {
    Date stopped;
}
