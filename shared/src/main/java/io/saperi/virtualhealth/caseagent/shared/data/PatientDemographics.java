package io.saperi.virtualhealth.caseagent.shared.data;

import lombok.Data;

import java.util.Date;

@Data
public class PatientDemographics {
    private String id;
    private String name;
    private String gender;
    private Date dob;
}
