package io.saperi.virtualhealth.caseagent.shared.event;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.saperi.virtualhealth.caseagent.shared.event.base.DeviceCommon;
import lombok.Data;

import java.util.Properties;

@Data
@JsonInclude(JsonInclude.Include. NON_NULL)
public class DeviceStatus extends DeviceCommon {
    //Configuration
    private Properties configuration;
    //Alarms
    private Properties alarms;
    //Settings
    private Properties settings;
    //State
    private Properties state;
}
