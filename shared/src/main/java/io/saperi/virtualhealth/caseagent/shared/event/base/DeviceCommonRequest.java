package io.saperi.virtualhealth.caseagent.shared.event.base;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include. NON_NULL)
abstract public class DeviceCommonRequest extends CaseCommonRequest {
    private String caseAgentId;

    private String deviceId;
    private String deviceType;
}
