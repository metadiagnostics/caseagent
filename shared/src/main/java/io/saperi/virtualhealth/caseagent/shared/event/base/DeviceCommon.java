package io.saperi.virtualhealth.caseagent.shared.event.base;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.Properties;

@Data
@JsonInclude(JsonInclude.Include. NON_NULL)
abstract public class DeviceCommon extends CaseCommon {
    private String deviceId;
    private String deviceType;

    private Properties deviceMetaData;
}
